import UIKit
import CoreSpotlight
import MobileCoreServices

class WeatherViewController: UIViewController, UISearchBarDelegate {
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var iconLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet var forecastViews: [ForecastView]!
    @IBOutlet weak var locationSearch: UISearchBar!
    
    let identifier = "WeatherIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationSearch.delegate = self
        viewModel = WeatherViewModel()
        viewModel?.startLocationService()
        setAccessibilityIdentifiers()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureLabels()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        configureLabelsWithAnimation()
    }
    
//    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
//        print("-----------------------")
//        print(searchText ?? "search bar instance is nil")
//        print("-----------------------")
//        locationLabel.text = searchText
//    }
//
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        let searchValue = searchBar.text
        viewModel?.getSearchWeather(searchText: searchValue!)
    }
    
    
    //didSet store previous state of the var
    var viewModel: WeatherViewModel? {
        didSet {
            setLocationLabel()
            viewModel?.iconText.observe {
                [unowned self] in
                self.iconLabel.text = $0
            }
            
            viewModel?.temperature.observe {
                [unowned self] in
                self.temperatureLabel.text = $0
            }
            setForcastView()
        }
    }
    
    func setAccessibilityIdentifiers() {
        locationLabel.accessibilityIdentifier = "a11y_current_city"
        iconLabel.accessibilityIdentifier = "a11y_wheather_icon"
        temperatureLabel.accessibilityIdentifier = "a11y_wheather_temperature"
    }
    
    func configureLabels(){
        locationLabel.center.x  -= view.bounds.width
        iconLabel.center.x -= view.bounds.width
        temperatureLabel.center.x -= view.bounds.width
        iconLabel.alpha = 0.0
        locationLabel.alpha = 0.0
        temperatureLabel.alpha = 0.0
    }
    
    func configureLabelsWithAnimation(){
        UIView.animate(withDuration: 0.5, animations: {
            self.locationLabel.center.x += self.view.bounds.width
        })
        
        UIView.animate(withDuration: 0.5, delay: 0.3, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.0, options: [], animations: {
            self.iconLabel.center.x += self.view.bounds.width
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.4, usingSpringWithDamping: 0.2, initialSpringVelocity: 0.0, options: [], animations: {
            self.temperatureLabel.center.x += self.view.bounds.width
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.3, options: [], animations: {
            self.iconLabel.alpha = 1.0
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.4, options: [], animations: {
            self.locationLabel.alpha = 1.0
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.5, options: [], animations: {
            self.temperatureLabel.alpha = 1.0
        }, completion: nil)
    }
    
    private func setLocationLabel() {
        viewModel?.location.observe {
            [unowned self] in
            self.locationLabel.text = $0
            
            let attributeSet = CSSearchableItemAttributeSet(itemContentType: kUTTypeText as String)
            attributeSet.title = self.locationLabel.text
            
            let item = CSSearchableItem(uniqueIdentifier: self.identifier, domainIdentifier: "com.rushjet.SwiftWeather", attributeSet: attributeSet)
            CSSearchableIndex.default().indexSearchableItems([item]){error in
                if let error =  error {
                    print("Indexing error: \(error.localizedDescription)")
                } else {
                    print("Location item successfully indexed")
                }
            }
        }
    }
    
    private func setForcastView() {
        viewModel?.forecasts.observe {
            [unowned self] (forecastViewModels) in
            if forecastViewModels.count >= 4 {
                for (index, forecastView) in self.forecastViews.enumerated() {
                    forecastView.loadViewModel(forecastViewModels[index])
                }
            }
        }
    }
}
