import Foundation
import CoreLocation


//protocol used as an interface (blueprint of class)
protocol LocationServiceProtocol {
  func locationDidUpdate(_ service: LocationService, location: CLLocation)
    func locationDidFail(withError error: SWError)
}

class LocationService: NSObject {
  var delegate: LocationServiceProtocol?

  fileprivate let locationManager = CLLocationManager()

  override init() {
    super.init()
    //delegate is here to receive update events => handled afterwards in the class
    locationManager.delegate = self
    
    //default accuracy -> more exists
    locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
  }

  // displays popup + get current location
  func requestLocation() {
    locationManager.requestWhenInUseAuthorization()
    locationManager.startUpdatingLocation()
  }
}

// Error handling + detect location changes
extension LocationService : CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    if let location = locations.first {
      print("Current location: \(location)")
      delegate?.locationDidUpdate(self, location: location)
    }
  }

  func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
    let swError = SWError(errorCode: .unableToFindLocation)
    delegate?.locationDidFail(withError: swError)
    print("Error finding location: \(error.localizedDescription)")
  }
}
