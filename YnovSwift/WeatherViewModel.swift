import Foundation
import CoreLocation

class WeatherViewModel {
  fileprivate let emptyString = ""

  let hasError: Observable<Bool>
  let errorMessage: Observable<String?>

  let location: Observable<String>
  let iconText: Observable<String>
  let temperature: Observable<String>
  let forecasts: Observable<[ForecastViewModel]>

  fileprivate var locationService: LocationService
  fileprivate var weatherService: WeatherServiceProtocol

  init() {
    hasError = Observable(false)
    errorMessage = Observable(nil)

    location = Observable(emptyString)
    iconText = Observable(emptyString)
    temperature = Observable(emptyString)
    forecasts = Observable([])

    locationService = LocationService()
    weatherService = OpenWeatherMapService()
  }

  func startLocationService() {
    locationService.delegate = self
    locationService.requestLocation()
  }
  
  func getSearchWeather(searchText: String) {
    weatherService.retrieveSearchWeatherInfo(searchText){(weather, error) -> Void in
      DispatchQueue.main.async(execute: {
        if let unwrappedError = error {
          print(unwrappedError)
          self.update(unwrappedError)
          return
        }
        
        guard let unwrappedWeather = weather else {
          return
        }
        self.update(unwrappedWeather)
      })
      }
  }
  

  fileprivate func update(_ weather: Weather) {
      hasError.value = false
      errorMessage.value = nil

      location.value = weather.location
      iconText.value = weather.iconText
      temperature.value = weather.temperature

      let tempForecasts = weather.forecasts.map { forecast in
        return ForecastViewModel(forecast)
      }
      forecasts.value = tempForecasts
  }

  fileprivate func update(_ error: SWError) {
      hasError.value = true

      switch error.errorCode {
      case .urlError:
        errorMessage.value = "The weather service is not working."
      case .networkRequestFailed:
        errorMessage.value = "The network appears to be down."
      case .jsonSerializationFailed:
        errorMessage.value = "We're having trouble processing weather data."
      case .jsonParsingFailed:
        errorMessage.value = "We're having trouble parsing weather data."
      case .unableToFindLocation:
        errorMessage.value = "We're having trouble getting user location."
      }
    
      location.value = emptyString
      iconText.value = emptyString
      temperature.value = emptyString
      self.forecasts.value = []
  }
}

extension WeatherViewModel: LocationServiceProtocol {
  func locationDidUpdate(_ service: LocationService, location: CLLocation) {
    weatherService.retrieveWeatherInfo(location) { (weather, error) -> Void in
      DispatchQueue.main.async(execute: {
        if let unwrappedError = error {
          print(unwrappedError)
          self.update(unwrappedError)
          return
        }

        guard let unwrappedWeather = weather else {
          return
        }
        self.update(unwrappedWeather)
      })
    }
  }
  
  func locationDidFail(withError error: SWError) {
      self.update(error)
  }
}
