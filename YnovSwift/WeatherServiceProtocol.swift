import Foundation
import CoreLocation

//Rename an existing type -> Can be Weather or Error
typealias WeatherCompletionHandler = (Weather?, SWError?) -> Void

protocol WeatherServiceProtocol {
  func retrieveWeatherInfo(_ location: CLLocation, completionHandler: @escaping WeatherCompletionHandler)
  func retrieveSearchWeatherInfo(_ location: String, completionHandler: @escaping WeatherCompletionHandler)
}
